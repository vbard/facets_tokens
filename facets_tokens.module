<?php

/**
 * @file
 * Token integration with facetapi.
 */

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Adds token settings settings to facet configuration.
 */
function facets_tokens_form_facetapi_facet_display_form_alter(&$form, &$form_state, $form_id) {
//  if (!user_access('administer facetapi pretty paths')) {
//    return;
//  }
  // Get global facet settings.
  $adapter = $form['#facetapi']['adapter'];
  $processor = new FacetapiUrlProcessorPrettyPaths($adapter);
  
  $facet = $form['#facetapi']['facet'];
  $facet_settings = $adapter->getFacetSettingsGlobal($facet);

  // Add facet token title option to global facet settings.
  $form['global']['facet_token_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Facet token title'),
    '#default_value' => !empty($facet_settings->settings['facet_token_title']) ? $facet_settings->settings['facet_token_title'] : '',
    '#description' => t('Token values will be prefixed with this title.'),
  );

  // Add facet token values delimiter option to global facet settings.
  $form['global']['facet_token_group_values_to_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Group facet values to one title'),
    '#default_value' => !empty($facet_settings->settings['facet_token_group_values_to_title']) ? TRUE : FALSE,
    '#description' => t('Token will be generated as "title value1 value2".'),
  );
  
  // Add facet token values delimiter option to global facet settings.
  $form['global']['facet_token_values_delimiter'] = array(
    '#type' => 'textfield',
    '#title' => t('Facet token values delimiter'),
    '#default_value' => !empty($facet_settings->settings['facet_token_values_delimiter']) ? $facet_settings->settings['facet_token_values_delimiter'] : '',
    '#description' => t('Token values will be delimited by this.'),
  );
}

/**
 * Helper function. Returns the same facet item with some additional info.
 * 
 * @param array $item
 *   The facet item array.
 * 
 * @return string $item
 *   The same facet item array with additional info elements.
 */
function _facets_tokens_get_facet_item_additional_info($item) {
  // For now will handle only fields with terms.
  $field_info = field_info_field($item['field alias']);
  if ($field_info['type'] !== 'taxonomy_term_reference') {
    return $item;
  }

  $terms = taxonomy_term_load_multiple(array($item['value']));
  if (empty($terms)) {
    return $item;
  }
  
  $term = reset($terms);

  $item['human_value'] = $term->name;
  
  $items[] = $item;

  return $item;
}

/**
 * Helper function. Returns all active facet items.
 * @param string $searcher_name
 *   The machine name of searcher.
 *
 * @return array
 */
function _facets_tokens_get_active_items($searcher_name) {
  if (!$searcher_name) {
    return array();
  }

  $adapter = facetapi_adapter_load($searcher_name);
  $active_items = $adapter->getAllActiveItems();

  return $active_items;
}

/**
 * Helper function. Returns searcher name to work with.
 * 
 * @return string
 */
function _facets_tokens_get_searcher_name() {
  $searchers = facetapi_get_searcher_info();
  
  if (!$searchers || empty($searchers)) {
    return array();
  }
  
  $keys = array_keys($searchers);
  $searcher_name = reset($keys);
  
  return $searcher_name;
}