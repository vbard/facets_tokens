<?php
/**
 * @file
 * Token callbacks for the facets_tokens module.
 */

/**
 * Implements hook_token_info().
 *
 * @ingroup facets_tokens
 */
function facets_tokens_token_info() {
  $searcher_name = _facets_tokens_get_searcher_name();
 
  $type = array(
    'name' => t('Facetapi tokens'),
    'description' => t('Tokens provided by active facetapi facets values.'),
  );
 
  $facetapi[$searcher_name] = array(
    'name' => t('Facet values for '.$searcher_name),
    'description' => t('Facet values for '.$searcher_name),
    'dynamic' => TRUE,
  );
 
  return array(
    'types' => array('facetapi' => $type),
    'tokens' => array('facetapi' => $facetapi),
  );
  
}


/**
 * Implements hook_tokens().
 *
 * @ingroup facets_tokens
 */
function facets_tokens_tokens($type, $tokens) {
  $replacements = array();

  if ($type != 'facetapi') {
    return $replacements;
  }

  $searcher_name = _facets_tokens_get_searcher_name();
  if (!$searcher_name) {
    return $replacements;
  }

  // 
  $facet_tokens = token_find_with_prefix($tokens, $searcher_name);
  if (!$facet_tokens || empty($facet_tokens)) {
    return $replacements;
  }

  $active_items = _facets_tokens_get_active_items($searcher_name);
  if (empty($active_items)) {
    return $replacements;
  }

  // Get token values that are set on that page.
  foreach ($facet_tokens as $name => $original) {

    // If 'all' token set - create it's value later.
    if ($name == 'all') {
      continue;
    }

    foreach ($active_items as $item_name => $item) {
      // Skip values not set as token on the page.
      if ($item['field alias'] !== $name) {
        continue;
      }

      $item = _facets_tokens_get_facet_item_additional_info($item);
      if (isset($item['human_value'])) {
        $replacements[$original] = $item['human_value'];
      }

      break;
    }
  }

  // If we does not have 'all' token set - we've done.
  if (!isset($facet_tokens['all'])) {
    return $replacements;
  }
  
  // Combine all facet values for 'all' token.
  // Group items by their facets.
  $grouped_facet_values = array();
  foreach ($active_items as $item_name => $item) {
    $item = _facets_tokens_get_facet_item_additional_info($item);

    if (!isset($item['human_value'])) {
      continue;
    }

     // Group facet items by facet name;
    $grouped_facet_values[$item['field alias']][] = $item;
  }
  
  if (empty($grouped_facet_values)) {
    return $replacements;
  }

  // Prepare to get facet settings.
  $searchers = facetapi_get_searcher_info();
  $searcher = $searchers[$searcher_name];
  $adapter = new SearchApiFacetapiAdapter($searcher);

  
  // Create strings from groups.
  $all_facet_values = array();
  foreach ($grouped_facet_values as $facet_name => $facet_items) {
    // Get facet settings.
    $facet = facetapi_facet_load($facet_name, $searcher_name);
    $facet_settings = $adapter->getFacetSettingsGlobal($facet);
  
    $title = empty($facet_settings->settings['facet_token_title']) ? ' ' : $facet_settings->settings['facet_token_title'];
    $group_values = empty($facet_settings->settings['facet_token_values_delimiter']) ? FALSE : TRUE;
    $delimiter = empty($facet_settings->settings['facet_token_values_delimiter']) ? ' ' : $facet_settings->settings['facet_token_values_delimiter'];

    $group_str = '';

    if ($group_values) {
      $group_str .= $title;
    }

    $i = 1;
    foreach ($facet_items as $item) {
      if (!$group_values) {
        $group_str .= $title;
      }

      $group_str .= $item['human_value'];

      if ($i < count($facet_items)) {
        $group_str .= $delimiter;
      }
     
      $i++;
    }
    
    $all_facet_values[] = $group_str;
  }

  $replacements[$facet_tokens['all']] = implode(' ', $all_facet_values);

  return $replacements;
}


